/**
 * Webpack configuration to insert into base config for production
 */

const path = require('path')
const webpack = require('webpack')

const baseConfig = require('./webpack.config.base')

module.exports = baseConfig({
  devtool: 'source-map',
  entry: [
    path.join(process.cwd(), 'src/index.js')
  ],
  output: {
    path: path.resolve(process.cwd(), './dist'), // Output our build files to dist directory
    filename: '[name].js',
    publicPath: ''
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }), // Use the production version of React when minifying
    new webpack.optimize.UglifyJsPlugin({minimize: true}), // Minify our JS bundles
  ],
  babelQuery: {
    presets: ['es2015', 'react', 'stage-0']
  }
})
