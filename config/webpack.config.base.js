/**
 * Base webpack config used by webpack.config.dev and webpack.config.prod
 */
const path = require('path')
const webpack = require('webpack')

module.exports = (options) => ({
  entry: options.entry,
  output: Object.assign({
    path: path.resolve(process.cwd(), 'dist'), // Compile into js/build.js
    publicPath: '/',
  }, options.output), // Merge with env dependent settings
  module: {
    loaders: [
    {
      test: /\.(js|jsx)$/,
      loader: 'babel-loader',
      include: path.resolve(process.cwd(), 'src'),
      exclude: /node_modules/,
      query: options.babelQuery
    },
    {
      test: /\.json$/,
      loader: 'json-loader',
    },
  ]},
  plugins: options.plugins,
  devtool: options.devtool,
  target: 'web',
  performance: options.performance || {},
})
