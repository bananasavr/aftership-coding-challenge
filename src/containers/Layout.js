/**
 * Layout.js
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'

class App extends Component {
  static propTypes = {
    children: React.PropTypes.node,
    params: React.PropTypes.object.isRequired,
    location: React.PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)
  }

  render() {
    const { props } = this

    return (
      <div className='page-container'>
        <main className='page-content'>
          {this.props.children}
        </main>
      </div>
    )
  }
}

export default (App)
