/**
 * Details.js
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getHistoricalCurrencyData,
  getLatestCurrencyData,
  getCurrencyList } from '../actions/actions'
import * as oxrSrvc from '../api/oxrSrvc'

import Tabs from '../components/tabs'
import Header from '../components/header'
import DatePicker from '../components/datepicker'

const mapStateToProps = (state) => ({
  currencyList: state.currencyList.items,
  latest: state.currencyData.latest,
  historical: state.currencyData.historical,
})

class Details extends Component {
  static propTypes = {
    params: React.PropTypes.object.isRequired,
    routeParams: React.PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this._handleTabClick = this._handleTabClick.bind(this)
    this._handleSearchInput = this._handleSearchInput.bind(this)
    this._handleDateInput = this._handleDateInput.bind(this)
    this.getHistorical = this.getHistorical.bind(this)
    this.getLatest = this.getLatest.bind(this)

    this.state = {
      activeTab: 'latest',
      searchInput: '',
      searchResult: [],
      showDate: false,
      dateInput: '',
      asyncLoading: true
    }
  }

  componentWillMount() {
    if (this.props.currencyList) {
      oxrSrvc.currencies((err, currencies) => {
        if (err) {
          console.log(err)
          return
        }
        this.props.getCurrencyList({
          currencyList: currencies
        })
      })
    }
    this.getLatest()
  }

  getLatest() {
    this.setState({ asyncLoading: true })
    oxrSrvc.latest({
      base: this.props.params.base
    }, (err, res) => {
      if (err) {
        console.log(err)
        return
      }
      this.props.getLatestCurrencyData({
        latest: res
      })
      this.setState({
        asyncLoading: false,
        searchInput: '',
        searchResult: Object.keys(res)
      })
    })
  }

  getHistorical(date) {
    this.setState({ asyncLoading: true })
    oxrSrvc.historical({
      date,
      base: this.props.params.base
    }, (err, res) => {
      if (err) {
        console.log(err)
        return
      }
      this.props.getHistoricalCurrencyData({
        historical: res
      })
      this.setState({
        asyncLoading: false,
        searchInput: '',
        searchResult: Object.keys(res)
      })
    })
  }

  _handleTabClick(tab) {
    if (!this.state.asyncLoading && this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        searchInput: '',
        searchResult: [],
        showDate: false
      })
      if (tab === 'latest') {
        this.getLatest()
      }
      if (tab === 'historical') {
        this.setState({
          showDate: true
        })
      }
    }
  }

  _handleSearchInput(e) {
    let value = e.target.value
    let result = this.search(value, this.props[this.state.activeTab])
    this.setState({
      searchInput: value,
      searchResult: result
    })
  }

  _handleDateInput(e) {
    let value = e.target.value
    this.setState({
      dateInput: value,
    })
  }

  search(query, list) {
    const symbolsSearchable = Object.keys(list)
    if (!query) {
      return symbolsSearchable
    }

    return _.filter(symbolsSearchable, (currency) => {
      const q = _.toLower(query)
      return (new RegExp(q)).test(_.toLower(currency))
    })
  }

  render() {
    const { props, state } = this

    return(
      <div>
        <Header
          country={props.currencyList[props.params.base]}
          base={props.params.base}
        />
        <Tabs
          active={state.activeTab}
          handleTabClick={this._handleTabClick}
        />
        {state.showDate ?
          <DatePicker
            handleDateSelect={this.getHistorical}
          />
        : null}
        <section className='search-container'>
          <span className='fa fa-search'></span>
          <input
            className='search-input'
            placeholder='Filter exchange rates...'
            type='text'
            onChange={this._handleSearchInput}
            value={this.state.searchInput}
          />
        </section>
        <section className='currency-list-container'>
          {state.searchResult.map((symbol) => {
            return(
              <div className='currency-list-card'>
                {symbol} : {props[state.activeTab][symbol]}
              </div>
            )
          })}
        </section>
      </div>
    )
  }
}

export default connect(mapStateToProps,
  { getHistoricalCurrencyData,
    getLatestCurrencyData,
    getCurrencyList })(Details)
