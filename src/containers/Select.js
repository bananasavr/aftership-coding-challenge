/**
 * Search.js
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import _ from 'lodash'
import { getCurrencyList } from '../actions/actions'
import * as oxrSrvc from '../api/oxrSrvc'

import Header from '../components/header'

const mapStateToProps = (state) => ({
  currencyList: state.currencyList.items
})

class Select extends Component {
  static propTypes = {
    currencyList: React.PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this._handleSearchInput = this._handleSearchInput.bind(this)

    this.state = {
      searchInput: '',
      searchResult: []
    }
  }

  componentWillMount() {
    oxrSrvc.currencies((err, currencies) => {
      if (err) {
        console.log(err)
        return
      }
      this.props.getCurrencyList({
        currencyList: currencies
      })
      this.setState({ searchResult: Object.keys(currencies) })
    })
  }

  _handleSearchInput(e) {
    let value = e.target.value
    let result = this.search(value, this.props.currencyList)
    this.setState({
      searchInput: value,
      searchResult: result
    })
  }

  search(query, currencyList) {
    const symbolsSearchable = Object.keys(currencyList)
    if (!query) {
      return symbolsSearchable
    }

    return _.filter(symbolsSearchable, (currency) => {
      const q = _.toLower(query)
      return (new RegExp(q)).test(_.toLower(currency))
    })
  }

  render() {
    const { props, state } = this

    return(
      <div>
        <Header />
        <section className='search-container'>
          <span className='fa fa-search'></span>
          <input className='search-input'
            placeholder='Find a currency...'
            type='text'
            onChange={this._handleSearchInput}
          />
        </section>
        <section className='currency-list-container'>
          {state.searchResult.map((currency) => {
            return(
              <Link className='currency-list-card' to={`/${currency}`}>{currency}</Link>
            )
          })}
        </section>
      </div>
    )
  }
}

export default connect(mapStateToProps, { getCurrencyList })(Select)
