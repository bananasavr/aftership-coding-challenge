/**
 * Redux actions.js
 * List of all actions for the app
 */

export const CURRENCY_LIST_GET = 'CURRENCY_LIST_GET'
export const CURRENCY_DATA_GET_HISTORICAL = 'CURRENCY_DATA_GET_HISTORICAL'
export const CURRENCY_DATA_GET_LATEST = 'CURRENCY_DATA_GET_LATEST'

// currency list
export function getCurrencyList(value) {
  return { type: CURRENCY_LIST_GET, value }
}

// currency data
export function getHistoricalCurrencyData(value) {
  return { type: CURRENCY_DATA_GET_HISTORICAL, value }
}

export function getLatestCurrencyData(value) {
  return { type: CURRENCY_DATA_GET_LATEST, value }
}
