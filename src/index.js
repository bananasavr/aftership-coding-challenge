import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import aftershipApp from './reducers/reducers'
export const aftershipStore = createStore(aftershipApp)

import routes from './routes';

render((
  <Provider store={aftershipStore}>
    <Router history={browserHistory}>
      {routes}
    </Router>
  </Provider>
  ), document.getElementById('root')
)
