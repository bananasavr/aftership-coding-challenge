/**
 * routes.js
 *
 * Application router
 */
import 'babel-polyfill'
import React from 'react'
import { Router, Route, IndexRoute, IndexRedirect, browserHistory } from 'react-router'

// Container components
import Layout from './containers/Layout'
import Select from './containers/Select'
import Details from './containers/Details'

export default (
  <Route path='/' component={Layout}>
    <IndexRoute component={Select} />
    <Route path=':base' component={Details} />
  </Route>
)
