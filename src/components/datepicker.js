/**
 * DatePicker Component
 */

import React, { Component } from 'react'

class DatePicker extends Component {
  constructor(props) {
    super(props)

    this._handleDateChange = this._handleDateChange.bind(this)

    this.state = {
      dateInput: ''
    }
  }

  _handleDateChange(e) {
    this.setState({ dateInput: e.target.value })
  }

  getTodayDate() {
    const today = new Date(),
      dd = today.getDate(),
      mm = today.getMonth() + 1,
      yyyy = today.getFullYear()

    return `${yyyy}-${ mm < 10 ? '0' + mm : mm }-${ dd < 10 ? '0' + dd : dd }`
  }

  render() {
    const { props, state } = this,
      maxDate = this.getTodayDate()

    return(
      <section className='date-container'>
        <label>Select a Date:</label>
        <input className='date-input' type='date' min='1999-01-01' max={maxDate} value={state.dateInput} onChange={this._handleDateChange} />
        <button onClick={(e) => { props.handleDateSelect(state.dateInput) }} disabled={!state.dateInput} >Go</button>
      </section>
    )
  }
}

export default DatePicker
