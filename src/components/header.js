/**
 * Header Component
 */

import React from 'react'
import { Link } from 'react-router'

export default (props) => (
  <section className='page-header'>
    {props.base
      ? <h1 className='header-title'>{`${props.country} : ${props.base}`}</h1>
      : <h1 className='header-title'>Select a Base Currency</h1>
    }
    {props.base
      ? <Link className='header-back' to='/'><span className='fa fa-undo'></span> Reset base currency</Link>
      : null
    }
  </section>
)
