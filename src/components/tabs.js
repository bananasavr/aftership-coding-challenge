/**
 * Tabs Component for Details page
 */

import React from 'react'

export default (props) => (
  <nav className='tabs-container'>
    <div className={`tab-item ${props.active === 'latest' ? 'active' : '' }`}
      onClick={(e) => { props.handleTabClick('latest') }}>Latest</div>
    <div className={`tab-item ${props.active === 'historical' ? 'active' : '' }`}
      onClick={(e) => { props.handleTabClick('historical') }}>Historical</div>
  </nav>
)
