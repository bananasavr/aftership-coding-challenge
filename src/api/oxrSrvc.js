import request from 'superagent'

export function currencies(callback) {
  request
    .get('/oxr/currencies')
    .end((err, res) => {
      callback(err, res.body)
    })
}

export function latest(options, callback) {
  return request
    .get('/oxr/latest')
    .query(options)
    .end((err, res) => {
      callback(err, res.body)
    })
}

export function historical(options, callback) {
  return request
    .get('/oxr/historical')
    .query(options)
    .end((err, res) => {
      callback(err, res.body)
    })
}
