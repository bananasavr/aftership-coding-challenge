import { CURRENCY_LIST_GET } from '../actions/actions'

const DEFAULT_STATE = {
  items: {}
}

const currencyList = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case CURRENCY_LIST_GET:
      return Object.assign({}, state, {
        items: action.value.currencyList
      })
      break
    default:
      return state
  }
}

export default currencyList
