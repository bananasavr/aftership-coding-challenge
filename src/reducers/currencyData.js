import { CURRENCY_DATA_GET_LATEST,
  CURRENCY_DATA_GET_HISTORICAL } from '../actions/actions'

const DEFAULT_STATE = {
  latest: {},
  historical: {},
  timerange: {},
  conversion: {}
}

const currencyData = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case CURRENCY_DATA_GET_LATEST:
      return Object.assign({}, state, {
        latest: action.value.latest
      })
      break
    case CURRENCY_DATA_GET_HISTORICAL:
      return Object.assign({}, state, {
        historical: action.value.historical
      })
      break
    default:
      return state
  }
}

export default currencyData
