import { combineReducers } from 'redux'

import currencyList from './currencyList'
import currencyData from './currencyData'

const aftershipApp = combineReducers({
  currencyList,
  currencyData
})

export default aftershipApp
