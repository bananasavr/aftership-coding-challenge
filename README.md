# Aftership Coding Challenge - Exchange Rate App

## Quick start

1. Copy the values within .env.example to .env file in the root path.
2. Run `npm install` to install dependencies for running dev server
3. Run `npm start` initiate our gulp and webpack build processes and start our express server
4. Navigate to `http://localhost:3000`

**Bundling:** Run `npm run build` to build bundle to `dist` directory

This project was build using Node v7.0.0

#### Example app hosted at:
https://intense-waters-10291.herokuapp.com/USD

## Project Constraints and Requirements

Exchange Rate Instructions:
> create a service that gives the latest and historical exchange rate for the currency.

This application leverages React-Redux on the front-end, ExpressJS and Node on the back-end, and data pulled from openexchangerates.org to grab the latest and historical exchange rate for a given currency. The `dotenv` npm package can be leveraged to help us manage all our environmental config values in a single `.env` file at the project root.

It is important to note that the hosted Heroku example app uses the free-tier Open Exchange Rates API key, and can only grab data for US Dollars. By signing up for a upgraded account, you can switch out the OXR_APP_ID value in your .env file to grab data for all other currencies.

## Project Structure
```
aftership
├── config
├── dist
├── node_modules
└── server
  └── routes
└── src
  ├── actions
  ├── api
  ├── components
  ├── containers
  ├── reducers
  ├── sass
  └── vendor
```

* `config` - where the base, development, and production webpack configurations will live
* `dist` - where the `src` files will be compiled into for production
* `server` - express server - for ease-of-use, I used the `babel-register` package in babelSever.js to allow usage of ES6 features on the server
  * `routes` - express router for calling open exchange rates endpoints
* `src` - where the meat of the application lives:
  * `actions` - Redux actions
  * `api` - service modules to integrate with external services
  * `components` - 'presentational' components
  * `containers` - 'container' components
  * `reducers` - Redux reducers
  * `sass` - application styling in sass
  * `vendor` - front end vendor modules to build into the `dist` folder

#### A note on Containers and Components
It is generally good practice to follow a container-component project structure, as it can be immensely useful to separate data fetching and rendering responsibilities.

In short, the `components` directory contains the presentational components - they are concerned with how things look, have no dependencies on the rest of the app (such as Flux/Redux actions and stores), rarely have state, and are not concerned with how the data is loaded or mutated.

The `containers` directory contains the container components - they are concerned with how things work, provide the data and behavior to serve to the presentational components, are stateful, and call Flux/Redux actions.

#### Scalability
The structure of this application was set up with scalability in mind. As the project and the engineering team grows, it is important that the application can also support tools such as react-redux, react-router, and redux-saga/redux-thunk to help us organize our codebase and enforce the flow of data through the application.

For this application, I used react-redux and react-router to help manage my application state and enforce the uni-directional flow of data. To give an overview both:
* React-Redux - used to manage the application state through a single application State Tree or Store. I also separated our presentational and container components because the React bindings for Redux embrace this separation.
* React-Router - As the application grows, more pages will be added and users may have a need to be able to navigate between pages with ease. The application routing file can be found at `src/routes.js`

Furthermore, the following tools should also be considered as the application grows:
* Redux-Saga - while I used superagent to help handle the API calls and prevent callback hell, redux sagas makes use of ES6 generator functions to make asynchronous effects in our React-Redux easier to read (makes the code look synchronous), manage and test. As the application and our directory of routes and endpoints grow, Redux-Saga can be leveraged to help manage our API calls.
