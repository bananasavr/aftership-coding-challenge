const path = require('path')
const gulp = require('gulp')
const gutil = require('gulp-util')
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const sourcemap = require('gulp-sourcemaps')
const imagemin = require('gulp-imagemin')
const del = require('del')

const webpack = require('webpack')

const compiler = webpack(require('./config/webpack.config.prod.js'))

// Clean out dist folder
gulp.task('clean', () => {
  return del(['dist/**/*'])
})

// Compile SASS files to CSS
gulp.task('build-sass', () => {
  return gulp.src(path.resolve(process.cwd(), 'src/sass/index.scss'))
    .pipe(sourcemap.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemap.write())
    .pipe(gulp.dest(path.resolve(process.cwd(), 'dist/')))
})

// Minify our images and copy to dist folder
gulp.task('images', () => {
  return gulp.src(path.resolve(process.cwd(), 'src/images/*'))
    .pipe(imagemin())
    .pipe(gulp.dest('dist/images'))
})

// Copy over fonts
gulp.task('vendor', () => {
  return gulp.src(path.resolve(process.cwd(), 'src/vendor/**/*'))
    .pipe(gulp.dest('dist/vendor'))
})

// Run webpack to bundle our JavaScript
gulp.task('build-app', () => {
  compiler.run((err, stats) => {
    if(err) {
        gutil.log('error', new gutil.PluginError('[webpack]', err))
    }
  })
})

gulp.task('default', ['images', 'vendor', 'build-sass', 'build-app'])
