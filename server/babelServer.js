/**
 * babelServer.js
 *
 * Entry point into server.js
 * babel-register package allows us to use 'import' and other ES6 features
 * with our server code
 */
require('dotenv').config();
require('babel-register')({
  presets: [ 'es2015', 'react', 'stage-0' ]
});
require('./server');
