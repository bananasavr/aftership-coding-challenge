import express from 'express'
import path from 'path'
import request from 'superagent'

const QUERY = { app_id: process.env.OXR_APP_ID }

let router = express.Router()

router.get('/', (req, res, next) => {
  res.json({ data: 'some data here' })
})

router.get('/currencies', (req, res, next) => {
  request
    .get(process.env.OXR_BASE_URL + 'currencies.json')
    .end((err, result) => {
      res.json(result.body)
    })
})

router.get('/latest', (req, res, next) => {
  request
    .get(process.env.OXR_BASE_URL + 'latest.json')
    .query(Object.assign(QUERY, req.query))
    .end((err, result) => {
      res.json(result.body.rates)
    })
})

router.get('/historical', (req, res, next) => {
  const q = {
    base: req.query.base
  }
  request
    .get(process.env.OXR_BASE_URL + `historical/${req.query.date}.json`)
    .query(Object.assign(QUERY, q))
    .end((err, result) => {
      res.json(result.body.rates)
    })
})

module.exports = router
