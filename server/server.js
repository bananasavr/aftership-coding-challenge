/**
 * server.js
 *
 */

import express from 'express'
import path from 'path'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'

import oxr from './routes/oxr'

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(process.cwd(), 'dist')))

app.set('port', (process.env.PORT || 5000))

app.listen(app.get('port'), () => {
	console.log('Hosting at port', app.get('port'))
})

app.use('/oxr', oxr)

app.get('*', function(req, res) {
  res.sendFile(path.join(process.cwd(), 'src/index.html'))
})

// Handle errors in development
app.use((err, req, res, next) => {
  res.status(err.status || 500)
  console.log(err.message)
})
